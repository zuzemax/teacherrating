package by.grsu.zezjlyma.teacherrating.controller;

import by.grsu.zezjlyma.teacherrating.model.UserDetailsImpl;
import by.grsu.zezjlyma.teacherrating.model.entity.Criterion;
import by.grsu.zezjlyma.teacherrating.model.entity.SubCriterion;
import by.grsu.zezjlyma.teacherrating.model.entity.TeacherInfo;
import by.grsu.zezjlyma.teacherrating.model.entity.UserSubCriterion;
import by.grsu.zezjlyma.teacherrating.repository.CriterionRepository;
import by.grsu.zezjlyma.teacherrating.repository.SubCriterionRepository;
import by.grsu.zezjlyma.teacherrating.repository.TeacherInfoRepo;
import by.grsu.zezjlyma.teacherrating.repository.UserRepository;
import by.grsu.zezjlyma.teacherrating.repository.UserSubCriterionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/criterion")
public class CriterionController {
    @Autowired
    private CriterionRepository criterionRepository;
    @Autowired
    private SubCriterionRepository subCriterionRepository;
    @Autowired
    private UserSubCriterionRepository userSubCriterionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TeacherInfoRepo teacherInfoRepo;

    @GetMapping
    public List<Criterion> getAll() {
        return criterionRepository.findAll();
    }

    @GetMapping("/sub")
    public List<SubCriterion> getAllSub() {
        return subCriterionRepository.findAll();
    }

    @PostMapping
    public Criterion save(@RequestBody Criterion criterion) {
        return criterionRepository.save(criterion);
    }

    @DeleteMapping
    public void delete(@RequestBody Criterion criterion) {
        criterionRepository.delete(criterion);
    }

    @GetMapping("/points/{userId}")
    public List<UserSubCriterion> getPoints(@PathVariable("userId") long userId) {
        return userSubCriterionRepository.findAllByUserId(userId);
    }

    @PostMapping("/point/{userId}")
    public void savePoint(@PathVariable("userId") long userId, @RequestBody UserSubCriterion point) {
        point.setUser(userRepository.findById(userId).get());
        UserSubCriterion.Key key = new UserSubCriterion.Key();
        key.setUserId(userId);
        key.setSubCriterionId(point.getSubCriterion().getId());
        point.setId(key);
        userSubCriterionRepository.save(point);
    }

    @GetMapping("/teacher/{userId}")
    public TeacherInfo getTeacherInfo(@PathVariable("userId") long userId) {
        return teacherInfoRepo.findById(userId).get();
    }

    @GetMapping("/sum/{userId}")
    public int getSum(@PathVariable("userId") long userId) {
        return getPoints(userId).stream().mapToInt(value -> value.getSubCriterion().getScore() * value.getPoints()).reduce(Integer::sum).getAsInt();
    }
}
