package by.grsu.zezjlyma.teacherrating.model.entity;

import by.grsu.zezjlyma.teacherrating.model.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = User.TABLE_USER)
public class User {
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_USER_ID = "USER_ID";
    public static final String COLUMN_EMAIL = "EMAIL";
    public static final String COLUMN_PASSWORD = "PASSWORD";
    public static final String COLUMN_USERNAME = "USERNAME";
    public static final String TABLE_USER_ROLE = "USER_ROLE";
    public static final String TABLE_USER = "USER";
    @Id
    @Column(name = COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = COLUMN_USERNAME, nullable = false, unique = true)
    private String username;
    @Column(name = COLUMN_PASSWORD, nullable = false)
    private String password;
    @Column(name = COLUMN_EMAIL, nullable = false)
    private String email;
    @Enumerated(EnumType.ORDINAL)
    @ElementCollection(targetClass = Role.class)
    @CollectionTable(name = TABLE_USER_ROLE, joinColumns = @JoinColumn(name = COLUMN_USER_ID, referencedColumnName = COLUMN_ID))
    private Collection<Role> roles;
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<UserSubCriterion> userSubCriteria;
}
