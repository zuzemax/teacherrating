package by.grsu.zezjlyma.teacherrating.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

@Getter
@Setter
@Entity
public class TeacherInfo {
    private static final String COLUMN_ID = "ID";
    @Id
    @Column(name = COLUMN_ID)
    private Long id;
    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;
    @Column
    private String faculty;
    @Column
    private String cathedra;
    @Column
    private String degree;
}
