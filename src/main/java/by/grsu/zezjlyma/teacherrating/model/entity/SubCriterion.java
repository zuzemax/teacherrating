package by.grsu.zezjlyma.teacherrating.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = SubCriterion.TABLE_SUB_CRITERION)
public class SubCriterion {
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_DESCRIPTION = "DESCRIPTION";
    public static final String COLUMN_SCORE = "SCORE";
    public static final String COLUMN_CRITERION_ID = "CRITERION_ID";
    public static final String TABLE_SUB_CRITERION = "SUB_CRITERION";
    @Id
    @Column(name = COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = COLUMN_DESCRIPTION, nullable = false)
    private String description;
    @Column(name = COLUMN_SCORE, nullable = false)
    private Integer score;
    @OneToMany(mappedBy = "subCriterion")
    @JsonIgnore
    private List<UserSubCriterion> userSubCriteria;
}
