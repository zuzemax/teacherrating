package by.grsu.zezjlyma.teacherrating.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = Criterion.TABLE_CRITERION)
public class Criterion {
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_DESCRIPTION = "DESCRIPTION";
    public static final String TABLE_CRITERION = "CRITERION";
    @Id
    @Column(name = COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = COLUMN_DESCRIPTION, nullable = false, unique = true)
    private String description;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = SubCriterion.COLUMN_CRITERION_ID, referencedColumnName = COLUMN_ID)
    private List<SubCriterion> subCriteria;
}
