package by.grsu.zezjlyma.teacherrating.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = UserSubCriterion.TABLE_USER_SUB_CRITERION)
public class UserSubCriterion {
    public static final String COLUMN_POINTS = "POINTS";
    public static final String COLUMN_SUB_CRITERION_ID = "SUB_CRITERION_ID";
    public static final String COLUMN_USER_ID = "USER_ID";
    public static final String TABLE_USER_SUB_CRITERION = "USER_SUB_CRITERION";
    @EmbeddedId
    private Key id;

    @Embeddable
    @EqualsAndHashCode
    @Getter
    @Setter
    public static class Key implements Serializable {
        @Column(name = COLUMN_SUB_CRITERION_ID)
        Long subCriterionId;
        @Column(name = COLUMN_USER_ID)
        Long userId;
    }

    @Column(name = COLUMN_POINTS, nullable = false)
    private Integer points;
    @ManyToOne
    @MapsId("subCriterionId")
    @JoinColumn(name = COLUMN_SUB_CRITERION_ID)
    private SubCriterion subCriterion;
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JoinColumn(name = COLUMN_USER_ID)
    @JsonIgnore
    private User user;
}
