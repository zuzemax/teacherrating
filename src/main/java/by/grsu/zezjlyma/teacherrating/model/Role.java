package by.grsu.zezjlyma.teacherrating.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ADMIN,
    EXPERT,
    GUEST;

    @Override
    public String getAuthority() {
        return this.name();
    }
}
