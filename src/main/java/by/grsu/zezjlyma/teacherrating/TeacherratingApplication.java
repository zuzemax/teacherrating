package by.grsu.zezjlyma.teacherrating;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeacherratingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeacherratingApplication.class, args);
	}

}
