package by.grsu.zezjlyma.teacherrating.service.impl;

import by.grsu.zezjlyma.teacherrating.model.UserDetailsImpl;
import by.grsu.zezjlyma.teacherrating.model.entity.User;
import by.grsu.zezjlyma.teacherrating.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private static final String USERNAME_NOT_FOUND_MESSAGE = "Failed to found user with username %s";
    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format(USERNAME_NOT_FOUND_MESSAGE, username)));
        return UserDetailsImpl.build(user);
    }
}
