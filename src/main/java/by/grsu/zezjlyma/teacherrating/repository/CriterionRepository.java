package by.grsu.zezjlyma.teacherrating.repository;

import by.grsu.zezjlyma.teacherrating.model.entity.Criterion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CriterionRepository extends JpaRepository<Criterion, Long> {
}