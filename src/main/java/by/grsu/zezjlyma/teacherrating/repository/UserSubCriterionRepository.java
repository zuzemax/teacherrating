package by.grsu.zezjlyma.teacherrating.repository;

import by.grsu.zezjlyma.teacherrating.model.entity.UserSubCriterion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserSubCriterionRepository extends JpaRepository<UserSubCriterion, UserSubCriterion.Key> {
    List<UserSubCriterion> findAllByUserId(long userId);
}
