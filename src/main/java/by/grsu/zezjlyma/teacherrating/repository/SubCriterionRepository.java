package by.grsu.zezjlyma.teacherrating.repository;

import by.grsu.zezjlyma.teacherrating.model.entity.SubCriterion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubCriterionRepository extends JpaRepository<SubCriterion, Long> {
}
