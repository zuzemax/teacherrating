package by.grsu.zezjlyma.teacherrating.repository;

import by.grsu.zezjlyma.teacherrating.model.entity.TeacherInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherInfoRepo extends JpaRepository<TeacherInfo, Long> {
}
